import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css'],
})
export class TableComponent {
  @Input() newUserData!: any[];

  isEditMode: boolean[] = [];
  editedData: any[] = [];
  
  pagination: number = 1;
  itemsPerPage: number = 4;

  enableEditMode(index: number) {
    this.isEditMode[index] = true;
    this.editedData[index] = { ...this.newUserData[index] };
  }

  cancelEditMode(index: number) {
    this.isEditMode[index] = false;
  }

  saveEditedData(index: number) {
    this.newUserData[index] = { ...this.editedData[index] };
    this.isEditMode[index] = false;
    alert('Data edited successfully');
  }

  deleteData(index: number) {
    index = this.itemsPerPage * (this.pagination - 1) + index;
    const confirmed: boolean = window.confirm(
      'Are you sure you want to delete this data?'
    );
    if (confirmed) {
      this.newUserData.splice(index, 1);
      alert('Data deleted successfully');
    } else {
      alert('Deletion cancelled');
    }
  }

  renderPage(page: number) {
    this.pagination = page;
  }
}
