import { Component } from '@angular/core';
import { Chart } from 'angular-highcharts';
import { aprResult, mayResult, junResult, julResult } from 'src/assets/data';

@Component({
  selector: 'app-pie-chart',
  templateUrl: './pie-chart.component.html',
  styleUrls: ['./pie-chart.component.css'],
})
export class PieChartComponent {
  result = julResult;
  chart!: Chart;

  constructor() {
    this.updateChart();
  }

  handleSelectChange(event: Event): void {
    const selectedValue = (event.target as HTMLSelectElement).value;
    switch (selectedValue) {
      case 'April':
        this.apr();
        break;
      case 'May':
        this.may();
        break;
      case 'June':
        this.jun();
        break;
      case 'July':
        this.jul();
        break;
      default:
        break;
    }
  }

  apr() {
    this.result = aprResult;
    this.updateChart();
  }

  may() {
    this.result = mayResult;
    this.updateChart();
  }

  jun() {
    this.result = junResult;
    this.updateChart();
  }

  jul() {
    this.result = julResult;
    this.updateChart();
  }

  updateChart() {
    this.chart = new Chart({
      chart: {
        type: 'pie',
      },
      title: {
        text: 'Pie chart',
      },
      series: [
        {
          type: 'pie',
          name: 'Data',
          data: this.result,
        },
      ],
    });
  }
}
