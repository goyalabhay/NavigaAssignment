import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'Naviga-Assignment';
  public userData: any[] = [
    {
      name: 'Abhay Goyal',
      email: 'abhay@gmail.com',
      phone: 1234567890,
      company: 'Naviga',
      city: 'Noida',
    },
    {
      name: 'ABC',
      email: 'ABC@gmail.com',
      phone: 9876543210,
      company: 'Mountblue',
      city: 'Bangalore',
    },
    {
      name: 'DEF',
      email: 'ABC@gmail.com',
      phone: 9876543210,
      company: 'Mountblue',
      city: 'Bangalore',
    },
    {
      name: 'NHJ',
      email: 'ABC@gmail.com',
      phone: 9876543210,
      company: 'Mountblue',
      city: 'Bangalore',
    },
    {
      name: 'IDJ',
      email: 'ABC@gmail.com',
      phone: 9876543210,
      company: 'Mountblue',
      city: 'Bangalore',
    },
    {
      name: 'JKC',
      email: 'ABC@gmail.com',
      phone: 9876543210,
      company: 'Mountblue',
      city: 'Bangalore',
    },
    {
      name: 'MNC',
      email: 'ABC@gmail.com',
      phone: 9876543210,
      company: 'Mountblue',
      city: 'Bangalore',
    },
    {
      name: 'MUY',
      email: 'ABC@gmail.com',
      phone: 9876543210,
      company: 'Mountblue',
      city: 'Bangalore',
    },
    {
      name: 'MMM',
      email: 'ABC@gmail.com',
      phone: 9876543210,
      company: 'Mountblue',
      city: 'Bangalore',
    },
    {
      name: 'ZZZ',
      email: 'ABC@gmail.com',
      phone: 9876543210,
      company: 'Mountblue',
      city: 'Bangalore',
    },
  ];
}
