import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css'],
})
export class FormComponent {
  @Input() newUserData!: any[];
  public newObjData: any = {};

  newData(
    newName: string,
    newEmail: string,
    newPhone: string,
    newCompany: string,
    newCity: string
  ): void {
    if (
      newName.trim() === '' ||
      newEmail.trim() === '' ||
      newPhone.trim() === '' ||
      newCompany.trim() === '' ||
      newCity.trim() === ''
    ) {
      alert('Please fill in all the fields.');
      return;
    }

    this.newObjData = {
      name: newName,
      email: newEmail,
      phone: newPhone,
      company: newCompany,
      city: newCity,
    };
    this.newUserData.push(this.newObjData);
    alert('New Data added successfully');
  }
}
