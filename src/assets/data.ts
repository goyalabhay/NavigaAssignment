const data = [
  {
    reporteeEmail: 'rahul.kumar@navigaglobal.com',
    reporteeName: 'Rahul Kumar',
    reporteeCode: 'R-N-12',
    lastPerformanceRating: 'Achiever',
    reporteeDepartment: 'MG2 Development - CoE',
    rmName: 'Mragna Gupta',
    managerId: '9000065',
    rmId: '3',
    reporteeTitle: 'Software Engineer',
    role: '',
    notActive: false,
    hrName: null,
    hrId: null,
    reporteeReview: [
      {
        reporteeReviewId: 11,
        reviewMonth: 'Jul',
        reviewYear: '2023',
        rmEmail: null,
        errIndex: 'NRP',
        reviewStatus: 'Submitted',
        reason: 'Not Applicable',
        dateOfLast1X1: '2023-07-06T00:00:00.000+00:00',
        commentByMng: 'ok',
        hrBpObservations: null,
        importanceForProject: 'known, Good player',
        actionPlans: 'No Action Plan',
        actionItems: 'no action',
        backupPerson: 'anurag',
        createdAt: '2023-07-07T09:31:05.600+00:00',
        updatedAt: '2023-07-07T09:31:05.600+00:00',
        skipRequired: false,
      },
    ],
    id: 7,
    employeeId: '7100097',
  },
  {
    reporteeEmail: 'test71@navigaglobal.com',
    reporteeName: 'test71',
    reporteeCode: 'R-N-711',
    lastPerformanceRating: null,
    reporteeDepartment: 'test',
    rmName: 'Mragna Gupta',
    managerId: null,
    rmId: '3',
    reporteeTitle: null,
    role: '',
    notActive: false,
    hrName: null,
    hrId: null,
    reporteeReview: [
      {
        reporteeReviewId: null,
        reviewMonth: 'Jul',
        reviewYear: '2023',
        rmEmail: '',
        errIndex: '',
        reviewStatus: 'Open',
        reason: '',
        dateOfLast1X1: null,
        commentByMng: '',
        hrBpObservations: '',
        importanceForProject: '',
        actionPlans: '',
        actionItems: '',
        backupPerson: '',
        createdAt: null,
        updatedAt: '2023-07-14T07:40:47.419+00:00',
        skipRequired: false,
      },
    ],
    id: 178,
    employeeId: null,
  },
  {
    reporteeEmail: 'test69@navigaglobal.com',
    reporteeName: 'test',
    reporteeCode: 'R-N-69',
    lastPerformanceRating: 'Achiever',
    reporteeDepartment: 'test',
    rmName: 'Mragna Gupta',
    managerId: null,
    rmId: '3',
    reporteeTitle: null,
    role: '',
    notActive: false,
    hrName: null,
    hrId: null,
    reporteeReview: [
      {
        reporteeReviewId: null,
        reviewMonth: 'Jul',
        reviewYear: '2023',
        rmEmail: '',
        errIndex: '',
        reviewStatus: 'Open',
        reason: '',
        dateOfLast1X1: null,
        commentByMng: '',
        hrBpObservations: '',
        importanceForProject: '',
        actionPlans: '',
        actionItems: '',
        backupPerson: '',
        createdAt: null,
        updatedAt: '2023-07-14T07:40:47.428+00:00',
        skipRequired: false,
      },
    ],
    id: 179,
    employeeId: null,
  },
  {
    reporteeEmail: 'test12121@gmail.com',
    reporteeName: 'test',
    reporteeCode: 'R-N-123',
    lastPerformanceRating: 'Over Achiever',
    reporteeDepartment: '',
    rmName: 'Mragna Gupta',
    managerId: null,
    rmId: '3',
    reporteeTitle: '',
    role: '',
    notActive: false,
    hrName: null,
    hrId: null,
    reporteeReview: [
      {
        reporteeReviewId: null,
        reviewMonth: 'Jul',
        reviewYear: '2023',
        rmEmail: '',
        errIndex: '',
        reviewStatus: 'Open',
        reason: '',
        dateOfLast1X1: null,
        commentByMng: '',
        hrBpObservations: '',
        importanceForProject: '',
        actionPlans: '',
        actionItems: '',
        backupPerson: '',
        createdAt: null,
        updatedAt: '2023-07-14T07:40:47.440+00:00',
        skipRequired: false,
      },
    ],
    id: 181,
    employeeId: null,
  },
  {
    reporteeEmail: 'test112@gmail.com',
    reporteeName: 'test112',
    reporteeCode: 'R-N-112',
    lastPerformanceRating: 'Role Model',
    reporteeDepartment: '',
    rmName: 'Mragna Gupta',
    managerId: null,
    rmId: '3',
    reporteeTitle: '',
    role: null,
    notActive: true,
    hrName: null,
    hrId: null,
    reporteeReview: [
      {
        reviewMonth: 'Jul',
        reporteeReviewId: null,
        reviewYear: '2023',
        rmEmail: '',
        errIndex: '',
        reviewStatus: 'Open',
        reason: '',
        dateOfLast1X1: null,
        commentByMng: '',
        hrBpObservations: '',
        importanceForProject: '',
        actionPlans: '',
        actionItems: '',
        backupPerson: '',
        createdAt: null,
        updatedAt: '2023-07-14T07:40:47.454+00:00',
        skipRequired: false,
      },
    ],
    id: 182,
    employeeId: null,
  },
  {
    reporteeEmail: 'test899@navigaglobal.com',
    reporteeName: 'test-Mragna',
    reporteeCode: 'R-N-899',
    lastPerformanceRating: null,
    reporteeDepartment: 'Development - Core - CoE',
    rmName: 'Mragna Gupta',
    managerId: null,
    rmId: '3',
    reporteeTitle: 'Analyst',
    role: '',
    notActive: false,
    hrName: null,
    hrId: null,
    reporteeReview: [
      {
        reporteeReviewId: null,
        reviewMonth: 'Jul',
        reviewYear: '2023',
        rmEmail: '',
        errIndex: '',
        reviewStatus: 'Open',
        reason: '',
        dateOfLast1X1: null,
        commentByMng: '',
        hrBpObservations: '',
        importanceForProject: '',
        actionPlans: '',
        actionItems: '',
        backupPerson: '',
        createdAt: null,
        updatedAt: '2023-07-14T07:40:47.464+00:00',
        skipRequired: false,
      },
    ],
    id: 183,
    employeeId: null,
  },
  {
    reporteeReview: [
      {
        reviewStatus: 'Open',
        reviewMonth: 'Apr',
      },
    ],
  },
  {
    reporteeReview: [
      {
        reviewStatus: 'Submitted',
        reviewMonth: 'Apr',
      },
    ],
  },
  {
    reporteeReview: [
      {
        reviewStatus: 'Open',
        reviewMonth: 'Apr',
      },
    ],
  },
  {
    reporteeReview: [
      {
        reviewStatus: 'Submitted',
        reviewMonth: 'Apr',
      },
    ],
  },
  {
    reporteeReview: [
      {
        reviewStatus: 'Submitted',
        reviewMonth: 'Apr',
      },
    ],
  },
  {
    reporteeReview: [
      {
        reviewStatus: 'Submitted',
        reviewMonth: 'Apr',
      },
    ],
  },
  {
    reporteeReview: [
      {
        reviewStatus: 'Open',
        reviewMonth: 'May',
      },
    ],
  },
  {
    reporteeReview: [
      {
        reviewStatus: 'Open',
        reviewMonth: 'May',
      },
    ],
  },
  {
    reporteeReview: [
      {
        reviewStatus: 'Open',
        reviewMonth: 'May',
      },
    ],
  },
  {
    reporteeReview: [
      {
        reviewStatus: 'Submitted',
        reviewMonth: 'May',
      },
    ],
  },
  {
    reporteeReview: [
      {
        reviewStatus: 'Submitted',
        reviewMonth: 'May',
      },
    ],
  },
  {
    reporteeReview: [
      {
        reviewStatus: 'Submitted',
        reviewMonth: 'May',
      },
    ],
  },
  {
    reporteeReview: [
      {
        reviewStatus: 'Submitted',
        reviewMonth: 'May',
      },
    ],
  },
  {
    reporteeReview: [
      {
        reviewStatus: 'Open',
        reviewMonth: 'Jun',
      },
    ],
  },
  {
    reporteeReview: [
      {
        reviewStatus: 'Open',
        reviewMonth: 'Jun',
      },
    ],
  },
  {
    reporteeReview: [
      {
        reviewStatus: 'Open',
        reviewMonth: 'Jun',
      },
    ],
  },
  {
    reporteeReview: [
      {
        reviewStatus: 'Open',
        reviewMonth: 'Jun',
      },
    ],
  },
  {
    reporteeReview: [
      {
        reviewStatus: 'Open',
        reviewMonth: 'Jun',
      },
    ],
  },

  {
    reporteeReview: [
      {
        reviewStatus: 'Submitted',
        reviewMonth: 'Jun',
      },
    ],
  },
  {
    reporteeReview: [
      {
        reviewStatus: 'Submitted',
        reviewMonth: 'Jun',
      },
    ],
  },
  {
    reporteeReview: [
      {
        reviewStatus: 'Submitted',
        reviewMonth: 'Jun',
      },
    ],
  },
  {
    reporteeReview: [
      {
        reviewStatus: 'Submitted',
        reviewMonth: 'Jun',
      },
    ],
  },
];

type ResultItem = [string, number][];

let aprResult: ResultItem = [
  ['Open', 0],
  ['Submitted', 0],
];
let mayResult: ResultItem = [
  ['Open', 0],
  ['Submitted', 0],
];
let junResult: ResultItem = [
  ['Open', 0],
  ['Submitted', 0],
];
let julResult: ResultItem = [
  ['Open', 0],
  ['Submitted', 0],
];

data.forEach((x) => {
  if (x.reporteeReview[0].reviewMonth === 'Apr') {
    if (x.reporteeReview[0].reviewStatus === 'Open') {
      aprResult[0][1] += 1;
    } else {
      aprResult[1][1] += 1;
    }
  } else if (x.reporteeReview[0].reviewMonth === 'May') {
    if (x.reporteeReview[0].reviewStatus === 'Open') {
      mayResult[0][1] += 1;
    } else {
      mayResult[1][1] += 1;
    }
  } else if (x.reporteeReview[0].reviewMonth === 'Jun') {
    if (x.reporteeReview[0].reviewStatus === 'Open') {
      junResult[0][1] += 1;
    } else {
      junResult[1][1] += 1;
    }
  } else if (x.reporteeReview[0].reviewMonth === 'Jul') {
    if (x.reporteeReview[0].reviewStatus === 'Open') {
      julResult[0][1] += 1;
    } else {
      julResult[1][1] += 1;
    }
  }
});

export { aprResult, mayResult, junResult, julResult };
